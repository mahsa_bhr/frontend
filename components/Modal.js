//show a blured page in which user enter his email and 
//with two buttons we lead the user 
//first button to wait a few second then close the modal
//and second button to close the modal imediately

import React from "react";

const Modal = props => {
  if (!props.show) {
    return null;
  }

  return (
    <div className="modal" onClick={props.onClose}>
      <div className="modal-content" onClick={e => e.stopPropagation()}>
        <label >
          {props.title}:
          <input className="email" placeholder= {`please enter your ${props.title}`} />
        </label>
        <button className="modal-btn cnfrm" onClick={e => e.stopPropagation()}>{`send confirmation ${props.title}`}</button>
        <button className="modal-btn" onClick={props.onClose}>close</button>
      </div>
    </div>
  );
}
export default Modal;
