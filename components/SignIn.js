//makes a form of id(or username) and email 
//checks whether the id exists in api or not 
//if not shows the error messeage and color the border of input fields
//it also has a loading option which disables buttons and is shown when fetching data

import Modal from "./Modal";
import { useState } from "react";

export default function SignIn() {
  const [showModal, setShowModal] = useState(false);
  const [data, setData] = useState({
    email: '',
    id: ''
  });
  /*
   const [id, setId] = useState("");
   const [email, setEmail] = useState("");
  const [errMsgShown, setErrMsgShown] = useState(false);
*/
  const [error, setError] = useState({
    emailerrmsg: '',
    logicalerrmsg: ''
  });
  const [isloading, setIsLoading] = useState(false);
  let button;

  const handleForgotClick = (e) => {
    e.preventDefault();
    setShowModal(true);
  }

  const onClose = (e) => {
    e.preventDefault();
    setShowModal(false);
  }

  const handleChange = (e) => {
    e.preventDefault();
    setData(prev => {
      return {
        ...prev,
        [e.target.name]: e.target.value
      }
    });
    if (e.target.name == 'email') {
      emailValidation();
    }
  }
  /*
    const handleIdChange = (e) => {
      setId(e.target.value);
    }
  
    const handleEmailChange = (e) => {
      setEmail(e.target.value);
    }
    */
  //here we check the email on change but it doesnt throw error when clicking the login button
  const emailValidation = () => {
    const regex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    if (data.email == '') {
      let errormsg = "email field is empty";
      setError(prev => {
        return {
          ...prev,
          emailerrmsg: errormsg
        }
      });
      return;
    }
    if (regex.test(data.email) === false) {
      let errormsg = "Email is not valid";
      setError(prev => {
        return {
          ...prev,
          emailerrmsg: errormsg
        }
      });
      return;
    }
    console.log('here');
    setError(prev => {
      return {
        ...prev,
        emailerrmsg: ''
      }
    });
    console.log(error.emailerrmsg);
    return;
  }

 
  async function handleLogin() {
    let url = "https://reqres.in/api/users/" + data.id;
    try {
      setIsLoading(true);
      const response = await fetch(url);
      if (!response.ok) {
        let errormsg = 'this user doesnt exist .create an accout.';
        setError(prev => {
          return {
            ...prev,
            logicalerrmsg: errormsg
          }
        });
        throw new Error(`This is an HTTP error: The status is ${response.status}`);
      }
      if (error.emailerrmsg != '') {
        throw new Error('Email has Error')
      }
      // emailValidation();
      setError({
        logicalerrmsg: '',
        emailerrmsg: ''
      });
      console.log(response);
    } catch (err) {
      console.log(err.message);
    } finally {
      setIsLoading(false);
    }
  }
  //username or id

  if (isloading) {
    button = <button onClick={handleLogin} className="signin-btn" disabled={isloading} >loading</button>
  } else {
    button = <button onClick={handleLogin} className="signin-btn" disabled={isloading} >login</button>
  }
  return (
    <div className="signin">
      <h2>welcome!</h2>
      <p className="login-title">login</p>
      <form className="form">
        <label className="label" for='id'>Username: </label>
        <input id='id' name="id" value={data.id} onChange={handleChange}
          className={(error.logicalerrmsg == '') ? "form-usr " : "form-usr error"} type={"text"} placeholder=" enter username" />
        <p className={error.logicalerrmsg == '' ? 'disapear' : 'error msg'}>{error.logicalerrmsg}</p>
        <label className="label" for='email'>Email:  </label>
        <input name="email" value={data.email} onChange={handleChange} id='email'
          className={(error.emailerrmsg == '') ? "form-email " : "form-email error"} type={"text"} placeholder="enter email" />
        <p className={error.emailerrmsg == '' ? 'disapear' : 'error msg'}>{error.emailerrmsg}</p>
        <a onClick={handleForgotClick}>Forgoten?</a><br /><br />
        <Modal
          title='email'
          onClose={onClose}
          show={showModal} />
      </form>
      {button}
      <button className="signin-btn" disabled={isloading}>Create an Account</button>
      <p className={((error.emailerrmsg == '') && (error.logicalerrmsg == '')) ? "" : "disapear"}>You're logged in!</p>
    </div>
  );
}