import './App.css';
import Register from './components/Register';
import './components/assets/styles/register.css';
import { Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Register />
    </div>
   
  );
}

export default App;
